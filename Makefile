remigrate:
	cd hassbrain_rest/ && ./remigrate.exp

populate_database:
	python3 hassbrain_rest/manage.py loaddata hassbrain_rest/dev/fixtures_debug2.json

create_dummys:
	/bin/bash hassbrain_rest/dev/create_dummys.sh

run:
	cd hassbrain_rest/ && bash runserver.sh


makemigrate:
	cd hassbrain_rest/. && python3 manage.py makemigrations
