import django
from django.conf.urls import url, include
from django.urls import path, re_path
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view
from backend import views
from hassbrain_web import settings
from hassbrain_web import settings as set

router = DefaultRouter()
router.register(set.URL_SERVER, views.ServerViewSet)
router.register(r'realtimenode', views.RealTimeNodeViewSet)
router.register(set.URL_DEVICE_PREDICTIONS, views.DevicePredictionViewSet)
router.register(set.URL_ACTIVITY_PREDICTIONS, views.ActivityPredictionViewSet)
router.register(set.URL_SYNTHETIC_ACTIVITY, views.SyntheticActivityViewSet)
router.register(r'devicecomponents', views.DeviceComponentViewSet)
router.register(r'persons', views.PersonViewSet)
#router.register(r'users', views.UserViewSet)
router.register(r'activities', views.ActivityViewSet)
router.register(r'devices', views.DeviceViewSet)
router.register(r'algorithms', views.AlgorithmViewSet)
router.register(r'models', views.ModelViewSet)
router.register(r'benchmarks', views.BenchmarkViewSet)
router.register(r'datasets', views.DatasetViewSet)
router.register(r'datainstances', views.DataInstanceViewSet)
router.register(r'smartphones', views.SmartphoneViewSet)
router.register(r'locations', views.LocationViewSet)
router.register(r'edges', views.EdgeViewSet)
urlpatterns = []

# API part
API_LINK='api/v1'

urlpatterns += [
#    url(r'^', include(router.urls)),
    url(r'^%s/'%(API_LINK), include(router.urls)),
]


# add coreapi suppoert
schema_view = get_schema_view(title='HASSBrain API')
urlpatterns+=[
#   url(r'^schema/$', schema_view),
   url(r'^%s/schema/$'%(API_LINK), schema_view),
]

# add auth support
urlpatterns += [
#    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^%s/auth/'%(API_LINK), include('rest_framework.urls')),
]

# serve media
# models can be downloaded
if settings.SERVE_MEDIA:
    urlpatterns += [
       url(r'^media/(?P<path>.*)$', django.views.static.serve,
           {'document_root': settings.MEDIA_ROOT}),
    ]
