export declare type AuthData = {
    hassUrl: string;
    clientId: string;
    expires: number;
    refresh_token: string;
    access_token: string;
    expires_in: number;
};
export declare type SaveTokensFunc = (data: AuthData | null) => void;
export declare type LoadTokensFunc = () => Promise<AuthData | null | undefined>;
export declare type getAuthOptions = {
    hassUrl?: string;
    clientId?: string;
    redirectUrl?: string;
    saveTokens?: SaveTokensFunc;
    loadTokens?: LoadTokensFunc;
};
export declare class Auth {
    private _saveTokens?;
    data: AuthData;
    constructor(data: AuthData, saveTokens?: SaveTokensFunc);
    readonly wsUrl: string;
    readonly accessToken: string;
    readonly expired: boolean;
    /**
     * Refresh the access token.
     */
    refreshAccessToken(): Promise<void>;
    /**
     * Revoke the refresh & access tokens.
     */
    revoke(): Promise<void>;
}
export declare function getAuth(options?: getAuthOptions): Promise<Auth>;
