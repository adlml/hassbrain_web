import { Store } from "./store";
import { Connection } from "./connection";
import { UnsubscribeFunc } from "./types";
export declare function createCollection<State>(key: string, fetchCollection: (conn: Connection) => Promise<State>, subscribeUpdates: (conn: Connection, store: Store<State>) => Promise<UnsubscribeFunc>, conn: Connection, onChange: (state: State) => void): UnsubscribeFunc;
