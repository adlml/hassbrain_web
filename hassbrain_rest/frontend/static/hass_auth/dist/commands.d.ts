import { Connection } from "./connection";
import { HassEntity, HassServices, HassConfig } from "./types";
export declare const getStates: (connection: Connection) => Promise<HassEntity[]>;
export declare const getServices: (connection: Connection) => Promise<HassServices>;
export declare const getConfig: (connection: Connection) => Promise<HassConfig>;
export declare const getUser: (connection: Connection) => Promise<HassConfig>;
export declare const callService: (connection: Connection, domain: string, service: string, serviceData?: object | undefined) => Promise<{}>;
