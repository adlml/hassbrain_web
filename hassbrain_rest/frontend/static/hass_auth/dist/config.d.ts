import { HassConfig, UnsubscribeFunc } from "./types";
import { Connection } from "./connection";
export declare const subscribeConfig: (conn: Connection, onChange: (state: HassConfig) => void) => UnsubscribeFunc;
