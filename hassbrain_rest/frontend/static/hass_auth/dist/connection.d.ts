import { ConnectionOptions, MessageBase } from "./types";
export declare type ConnectionEventListener = (conn: Connection, eventData?: any) => void;
declare type Events = "ready" | "disconnected" | "reconnect-error";
declare type SubscribeEventCommmandInFlight = {
    resolve: (result?: any) => void;
    reject: (err: any) => void;
    eventCallback: (ev: any) => void;
    eventType?: string;
    unsubscribe: () => Promise<void>;
};
declare type CommandWithAnswerInFlight = {
    resolve: (result?: any) => void;
    reject: (err: any) => void;
};
declare type CommandInFlight = SubscribeEventCommmandInFlight | CommandWithAnswerInFlight;
export declare class Connection {
    options: ConnectionOptions;
    commandId: number;
    commands: {
        [commandId: number]: CommandInFlight;
    };
    eventListeners: {
        [eventType: string]: ConnectionEventListener[];
    };
    closeRequested: boolean;
    socket: WebSocket;
    constructor(socket: WebSocket, options: ConnectionOptions);
    setSocket(socket: WebSocket): void;
    addEventListener(eventType: Events, callback: ConnectionEventListener): void;
    removeEventListener(eventType: Events, callback: ConnectionEventListener): void;
    fireEvent(eventType: Events, eventData?: any): void;
    close(): void;
    subscribeEvents<EventType>(eventCallback: (ev: EventType) => void, eventType?: string): Promise<() => Promise<void>>;
    ping(): Promise<{}>;
    sendMessage(message: MessageBase, commandId?: number): void;
    sendMessagePromise<Result>(message: MessageBase): Promise<Result>;
    private _handleMessage;
    private _handleClose;
    private _genCmdId;
}
export {};
