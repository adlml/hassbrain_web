import { HassEntities, UnsubscribeFunc } from "./types";
import { Connection } from "./connection";
export declare const subscribeEntities: (conn: Connection, onChange: (state: HassEntities) => void) => UnsubscribeFunc;
