import { HassServices, UnsubscribeFunc } from "./types";
import { Connection } from "./connection";
export declare const subscribeServices: (conn: Connection, onChange: (state: HassServices) => void) => UnsubscribeFunc;
