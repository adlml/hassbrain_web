import { ConnectionOptions } from "./types";
export declare function createSocket(options: ConnectionOptions): Promise<WebSocket>;
