import { UnsubscribeFunc } from "./types";
declare type Listener<State> = (state: State) => void;
declare type NoSubscribersCallback = () => void;
export declare class Store<State> {
    private _noSub;
    listeners: Listener<State>[];
    state: State | undefined;
    constructor(noSubscriptions: NoSubscribersCallback);
    /**
     * Create a bound copy of the given action function.
     * The bound returned function invokes action() and persists the result back to the store.
     * If the return value of `action` is a Promise, the resolved value will be used as state.
     * @param {Function} action An action of the form `action(state, ...args) -> stateUpdate`
     * @returns {Function} boundAction()
     */
    action(action: (state: State, ...args: any[]) => Partial<State> | Promise<Partial<State>> | null): (...args: any[]) => void | Promise<void>;
    setState(update: Partial<State>, overwrite?: boolean): void;
    /**
     * Register a listener function to be called whenever state is changed. Returns an `unsubscribe()` function.
     * @param {Function} listener A function to call when state changes. Gets passed the new state.
     * @returns {Function} unsubscribe()
     */
    subscribe(listener: Listener<State>): UnsubscribeFunc;
    /**
     * Remove a previously-registered listener function.
     * @param {Function} listener The callback previously passed to `subscribe()` that should be removed.
     * @function
     */
    unsubscribe(listener: Listener<State>): void;
}
export {};
