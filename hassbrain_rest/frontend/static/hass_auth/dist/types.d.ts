import { Auth } from "./auth";
export declare type Error = 1 | 2 | 3 | 4;
export declare type UnsubscribeFunc = () => void;
export declare type ConnectionOptions = {
    setupRetry: number;
    auth?: Auth;
    createSocket: (options: ConnectionOptions) => Promise<WebSocket>;
};
export declare type MessageBase = {
    id?: number;
    type: string;
    [key: string]: any;
};
export declare type HassEventBase = {
    origin: string;
    time_fired: string;
    context: {
        id: string;
        user_id: string;
    };
};
export declare type HassEvent = HassEventBase & {
    event_type: string;
    data: {
        [key: string]: any;
    };
};
export declare type StateChangedEvent = HassEventBase & {
    event_type: "state_changed";
    data: {
        entity_id: string;
        new_state: HassEntity | null;
        old_state: HassEntity | null;
    };
};
export declare type HassConfig = {
    latitude: number;
    longitude: number;
    elevation: number;
    unit_system: {
        length: string;
        mass: string;
        volume: string;
        temperature: string;
    };
    location_name: string;
    time_zone: string;
    components: string[];
    config_dir: string;
    whitelist_external_dirs: string[];
    version: string;
};
export declare type HassEntityBase = {
    entity_id: string;
    state: string;
    last_changed: string;
    last_updated: string;
};
export declare type HassEntityAttributeBase = {
    friendly_name?: string;
    unit_of_measurement?: string;
    icon?: string;
    entity_picture?: string;
    supported_features?: number;
    hidden: boolean;
    assumed_state?: boolean;
    device_class?: string;
};
export declare type HassEntity = HassEntityBase & {
    attributes: HassEntityAttributeBase & {
        [key: string]: any;
    };
};
export declare type HassEntities = {
    [entity_id: string]: HassEntity;
};
export declare type HassService = {
    description: string;
    fields: {
        [field_name: string]: {
            description: string;
            example: string;
        };
    };
};
export declare type HassDomainServices = {
    [service_name: string]: HassService;
};
export declare type HassServices = {
    [domain: string]: HassDomainServices;
};
