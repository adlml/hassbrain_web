#!/usr/bin/bash
rm -f dev/db.sqlite3
rm -r backend/migrations
#rm -r frontend/migrations
rm -rf backend/__pycache__
rm -rf frontend/__pycache__
#python3 manage.py makemigrations backend frontend
python3.7 manage.py makemigrations backend
python3.7 manage.py migrate
python3.7 manage.py migrate --run-syncdb
python3.7 manage.py createsuperuser --email test@test.de --username admin
python3.7 manage.py createsuperuser --email test@test.de --username frontend
python3.7 manage.py runserver
