set nocompatible
filetype off " required for vundle
"+----------: THE ULTIMATIVE VIM CONFIGURATION FILE
" Vundle configuration {{{
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" Vundle manages Vundle
Plugin 'VundleVim/Vundle.vim'
" original repos on github
Plugin 'altercation/vim-colors-solarized'
" Plugin 'vim-syntastic/syntastic'
Plugin 'joshdick/onedark.vim'
" Plugin 'powerline/powerline'
" Plugin 'Lokaltog/powerline'
" Plugin 'scrooloose/nerdtree'
" Plugin 'tpope/vim-fugitive' " Git Wrapper, doing git stuff in vim
" Plugin 'lervag/vimtex'
call vundle#end()           
filetype plugin indent on    " required
" }}}
" Misc {{{
set clipboard=unnamed "makes system clipboard accessable from vim
" }}}
" Spaces and Tabs {{{ 
set tabstop=2      " number of visual spaces per TAB
set softtabstop=2  "number of visual spaces in TAB when editing
set expandtab      "tabs are space
" }}}
" UI Config {{{ 
set number         " show line numbers
set showcmd        " show command in bottom bar
set cursorline     "horizontal highlight under current line
filetype indent on "load filetype-specific indent files
"set wildmenue      " visual autocomplete for command menu
" set lazyredraw     " redraw only when we need to. todo maybe the fuckup
" }}}
" Searching {{{ 
set showmatch      "highlight paranthese matching ()
set incsearch      " search as characters are entered
set hlsearch       "highlight matches
nnoremap <leader><space>:nohlsearch<CR> " press space to disable search highlighting
" }}}
" Folding {{{ "
" set foldenable          " enable folding
" set foldlevelstart=10   " open most folds by default
" set foldnestmax=10      " 10 nested fold max
" " space open/closes folds
" nnoremap <space> za     
" set foldmethod=indent   " fold based on indent level
" }}}
" Custom Movements {{{
" move vertically by visual line
nnoremap j gj
nnoremap k gk

" move to beginning/end of line
nnoremap B ^
nnoremap E $

" $/^ doesn't do anything
nnoremap $ <nop>
nnoremap ^ <nop>

" highlight last inserted text
nnoremap gV `[v`]
" }}}
" Leader Shortcuts {{{ 
" jk is escape
inoremap jk <esc> 

" disable auto indendation of comments 
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" edit vimrc/zshrc and load vimrc bindings
nnoremap <leader>ev :vsp $MYVIMRC<CR>
"nnoremap <leader>ez :vsp ~/.zshrc<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>
" save session
nnoremap <leader>s :mksession<CR>
""" }}}
" CtrlP Setting {{{ 
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'
" }}}
" Autogroups {{{
" language specific settings for file extensions 
augroup configgroup
    autocmd!
    autocmd VimEnter * highlight clear SignColumn
    autocmd BufWritePre *.php,*.py,*.js,*.txt,*.hs,*.java,*.md
                \:call <SID>StripTrailingWhitespaces()
    autocmd FileType java setlocal noexpandtab
    autocmd FileType java setlocal list
    autocmd FileType java setlocal listchars=tab:+\ ,eol:-
    autocmd FileType java setlocal formatprg=par\ -w80\ -T4
    autocmd FileType php setlocal expandtab
    autocmd FileType php setlocal list
    autocmd FileType php setlocal listchars=tab:+\ ,eol:-
    autocmd FileType php setlocal formatprg=par\ -w80\ -T4
    autocmd FileType ruby setlocal tabstop=2
    autocmd FileType ruby setlocal shiftwidth=2
    autocmd FileType ruby setlocal softtabstop=2
    autocmd FileType ruby setlocal commentstring=#\ %s
    autocmd FileType python setlocal commentstring=#\ %s
    autocmd BufEnter *.cls setlocal filetype=java
    autocmd BufEnter *.zsh-theme setlocal filetype=zsh
    autocmd BufEnter Makefile setlocal noexpandtab
    autocmd BufEnter *.sh setlocal tabstop=2
    autocmd BufEnter *.sh setlocal shiftwidth=2
    autocmd BufEnter *.sh setlocal softtabstop=2
augroup END
" }}}
" Backups {{{
" move backups to temp folder
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup
"}}}
" Custom Functions {{{
" strips trailing whitespace at the end of files. this
" is called on buffer write in the autogroup above.
function! <SID>StripTrailingWhitespaces()
    " save last search & cursor position
    let _s=@/
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    let @/=_s
    call cursor(l, c)
endfunction

" }}}
" Organisation {{{
" set number
"foldmethod=marker
"foldlevel=0
"set modelines=1
" vim:foldmethod=marker:foldlevel=0
" }}}
" Plugin {{{
" vimtex
let g:latex_view_general_viewer = 'zathura'
let g:vimtex_view_method = "zathura"
" Syntastic {{{
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
" }}}
"
" }}}
" Solarized Dark
" Colors {{{ 
syntax enable "enable syntax processing
set t_co=256
set background=dark
colorscheme solarized
filetype plugin on 
filetype indent on
set guifont=Consolas:h12
" }}}
